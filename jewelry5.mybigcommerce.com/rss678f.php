<?xml version="1.0" encoding="UTF-8"?>
<rss version="2.0" xmlns:content="http://purl.org/rss/1.0/modules/content/" xmlns:dc="http://purl.org/dc/elements/1.1/"  xmlns:isc="http://dtd.interspire.com/rss/isc-1.0.dtd">
	<channel>
		<title><![CDATA[Jewelry: Product Search]]></title>
		<link>https://jewelry5.mybigcommerce.com</link>
		<description><![CDATA[A custom search for products at Jewelry.]]></description>
		<pubDate>Fri, 08 May 2020 09:46:49 +0000</pubDate>
		<isc:store_title><![CDATA[Jewelry]]></isc:store_title>
		<item>
			<title><![CDATA[[Sample&91; Oak Cheese Grater]]></title>
			<link>https://jewelry5.mybigcommerce.com/oak-cheese-grater/</link>
			<pubDate>Fri, 03 Jul 2015 18:31:23 +0000</pubDate>
			<guid isPermaLink="false">https://jewelry5.mybigcommerce.com/oak-cheese-grater/</guid>
			<description><![CDATA[<div style='float: right; padding: 10px;'><a href="https://jewelry5.mybigcommerce.com/oak-cheese-grater/"><img src="https://cdn11.bigcommerce.com/s-vkapp4pk65/products/94/images/408/06__14965.1492012183.190.285.jpg?c=2" alt="[Sample&91; Oak Cheese Grater" /></a></div><p>Crafted from oak and stainless steel this handy cheese grater is the perfect addition for the considered tabletop. Works wonderfully for grated parmesan: the fine stainless steel teeth grate the thing strings of cheese into the drawer below allowing guests to pinch as needed.</p>
<p>Measures 1..<p><strong>Price: $34.95</strong> <img src="https://cdn11.bigcommerce.com/r-7f3397d2ae83e8b48dd889540b7b618246f07f43/themes/ClassicNext/images/IcoRating0.gif" alt="" /></p>]]></description>
			<content:encoded><![CDATA[<div style='float: right; padding: 10px;'><a href="https://jewelry5.mybigcommerce.com/oak-cheese-grater/"><img src="https://cdn11.bigcommerce.com/s-vkapp4pk65/products/94/images/408/06__14965.1492012183.190.285.jpg?c=2" alt="[Sample&91; Oak Cheese Grater" /></a></div><p>Crafted from oak and stainless steel this handy cheese grater is the perfect addition for the considered tabletop. Works wonderfully for grated parmesan: the fine stainless steel teeth grate the thing strings of cheese into the drawer below allowing guests to pinch as needed.</p>
<p>Measures 1..<p><strong>Price: $34.95</strong> <img src="https://cdn11.bigcommerce.com/r-7f3397d2ae83e8b48dd889540b7b618246f07f43/themes/ClassicNext/images/IcoRating0.gif" alt="" /></p>]]></content:encoded>
			<isc:description><![CDATA[<p>Crafted from oak and stainless steel this handy cheese grater is the perfect addition for the considered tabletop. Works wonderfully for grated parmesan: the fine stainless steel teeth grate the thing strings of cheese into the drawer below allowing guests to pinch as needed.</p>
<p>Measures 1..]]></isc:description>
			<isc:productid><![CDATA[94]]></isc:productid>
			<isc:thumb><![CDATA[https://cdn11.bigcommerce.com/s-vkapp4pk65/products/94/images/408/06__14965.1492012092.190.285.jpg?c=2]]></isc:thumb>
			<isc:image><![CDATA[https://cdn11.bigcommerce.com/s-vkapp4pk65/products/94/images/408/06__14965.1492012092.1280.1280.jpg?c=2]]></isc:image>
			<isc:price><![CDATA[$34.95]]></isc:price>
			<isc:rating><![CDATA[0]]></isc:rating>
			<isc:rating_image><![CDATA[https://cdn11.bigcommerce.com/r-7f3397d2ae83e8b48dd889540b7b618246f07f43/themes/ClassicNext/images/IcoRating0.gif]]></isc:rating_image>
		</item>
	</channel>
</rss>
