<?xml version="1.0" encoding="UTF-8"?>
<rss version="2.0" xmlns:content="http://purl.org/rss/1.0/modules/content/" xmlns:dc="http://purl.org/dc/elements/1.1/"  xmlns:isc="http://dtd.interspire.com/rss/isc-1.0.dtd">
	<channel>
		<title><![CDATA[Jewelry: Product Search]]></title>
		<link>https://jewelry5.mybigcommerce.com</link>
		<description><![CDATA[A custom search for products at Jewelry.]]></description>
		<pubDate>Fri, 08 May 2020 09:50:25 +0000</pubDate>
		<isc:store_title><![CDATA[Jewelry]]></isc:store_title>
		<item>
			<title><![CDATA[[Sample&91; Smith Journal 13]]></title>
			<link>https://jewelry5.mybigcommerce.com/smith-journal-13/</link>
			<pubDate>Fri, 03 Jul 2015 20:55:30 +0000</pubDate>
			<guid isPermaLink="false">https://jewelry5.mybigcommerce.com/smith-journal-13/</guid>
			<description><![CDATA[<div style='float: right; padding: 10px;'><a href="https://jewelry5.mybigcommerce.com/smith-journal-13/"><img src="https://cdn11.bigcommerce.com/s-vkapp4pk65/products/111/images/402/01__52909.1492012112.190.285.jpg?c=2" alt="[Sample&91; Smith Journal 13" /></a></div><p>Volume 13 of Smith Journal is crammed with more than its fair share of sharp minds. Top of the list would have to be Solomon Shereshevsky, who remembered every single thing he&rsquo;d ever come across &ndash; a great skill to have when it came to party tricks, but enough to send him crackers. A..<p><strong>Price: <span class="SalePrice">$22.00</span></strong> <img src="https://cdn11.bigcommerce.com/r-7f3397d2ae83e8b48dd889540b7b618246f07f43/themes/ClassicNext/images/IcoRating4.gif" alt="" /></p>]]></description>
			<content:encoded><![CDATA[<div style='float: right; padding: 10px;'><a href="https://jewelry5.mybigcommerce.com/smith-journal-13/"><img src="https://cdn11.bigcommerce.com/s-vkapp4pk65/products/111/images/402/01__52909.1492012112.190.285.jpg?c=2" alt="[Sample&91; Smith Journal 13" /></a></div><p>Volume 13 of Smith Journal is crammed with more than its fair share of sharp minds. Top of the list would have to be Solomon Shereshevsky, who remembered every single thing he&rsquo;d ever come across &ndash; a great skill to have when it came to party tricks, but enough to send him crackers. A..<p><strong>Price: <span class="SalePrice">$22.00</span></strong> <img src="https://cdn11.bigcommerce.com/r-7f3397d2ae83e8b48dd889540b7b618246f07f43/themes/ClassicNext/images/IcoRating4.gif" alt="" /></p>]]></content:encoded>
			<isc:description><![CDATA[<p>Volume 13 of Smith Journal is crammed with more than its fair share of sharp minds. Top of the list would have to be Solomon Shereshevsky, who remembered every single thing he&rsquo;d ever come across &ndash; a great skill to have when it came to party tricks, but enough to send him crackers. A..]]></isc:description>
			<isc:productid><![CDATA[111]]></isc:productid>
			<isc:thumb><![CDATA[https://cdn11.bigcommerce.com/s-vkapp4pk65/products/111/images/402/01__52909.1492012060.190.285.jpg?c=2]]></isc:thumb>
			<isc:image><![CDATA[https://cdn11.bigcommerce.com/s-vkapp4pk65/products/111/images/402/01__52909.1492012060.1280.1280.jpg?c=2]]></isc:image>
			<isc:price><![CDATA[<span class="SalePrice">$22.00</span>]]></isc:price>
			<isc:rating><![CDATA[4]]></isc:rating>
			<isc:rating_image><![CDATA[https://cdn11.bigcommerce.com/r-7f3397d2ae83e8b48dd889540b7b618246f07f43/themes/ClassicNext/images/IcoRating4.gif]]></isc:rating_image>
		</item>
		<item>
			<title><![CDATA[[Sample&91; Canvas Laundry Cart]]></title>
			<link>https://jewelry5.mybigcommerce.com/canvas-laundry-cart/</link>
			<pubDate>Fri, 03 Jul 2015 18:48:07 +0000</pubDate>
			<guid isPermaLink="false">https://jewelry5.mybigcommerce.com/canvas-laundry-cart/</guid>
			<description><![CDATA[<div style='float: right; padding: 10px;'><a href="https://jewelry5.mybigcommerce.com/canvas-laundry-cart/"><img src="https://cdn11.bigcommerce.com/s-vkapp4pk65/products/103/images/405/04__32061.1492012148.190.285.jpg?c=2" alt="[Sample&91; Canvas Laundry Cart" /></a></div><div>
<p>The last laundry cart you'll ever buy. This industrial non-collapsable cart is constructed with a steel framed body using heavyweight canvas capped with a soft leather rim sitting on four smooth rolling casters. Keeps the grubby garments off the floor and almost makes laundry enjoyable. ..<p><strong>Price: <span class="SalePrice">$200.00</span></strong> <img src="https://cdn11.bigcommerce.com/r-7f3397d2ae83e8b48dd889540b7b618246f07f43/themes/ClassicNext/images/IcoRating4.gif" alt="" /></p>]]></description>
			<content:encoded><![CDATA[<div style='float: right; padding: 10px;'><a href="https://jewelry5.mybigcommerce.com/canvas-laundry-cart/"><img src="https://cdn11.bigcommerce.com/s-vkapp4pk65/products/103/images/405/04__32061.1492012148.190.285.jpg?c=2" alt="[Sample&91; Canvas Laundry Cart" /></a></div><div>
<p>The last laundry cart you'll ever buy. This industrial non-collapsable cart is constructed with a steel framed body using heavyweight canvas capped with a soft leather rim sitting on four smooth rolling casters. Keeps the grubby garments off the floor and almost makes laundry enjoyable. ..<p><strong>Price: <span class="SalePrice">$200.00</span></strong> <img src="https://cdn11.bigcommerce.com/r-7f3397d2ae83e8b48dd889540b7b618246f07f43/themes/ClassicNext/images/IcoRating4.gif" alt="" /></p>]]></content:encoded>
			<isc:description><![CDATA[<div>
<p>The last laundry cart you'll ever buy. This industrial non-collapsable cart is constructed with a steel framed body using heavyweight canvas capped with a soft leather rim sitting on four smooth rolling casters. Keeps the grubby garments off the floor and almost makes laundry enjoyable. ..]]></isc:description>
			<isc:productid><![CDATA[103]]></isc:productid>
			<isc:thumb><![CDATA[https://cdn11.bigcommerce.com/s-vkapp4pk65/products/103/images/405/04__32061.1492012077.190.285.jpg?c=2]]></isc:thumb>
			<isc:image><![CDATA[https://cdn11.bigcommerce.com/s-vkapp4pk65/products/103/images/405/04__32061.1492012077.1280.1280.jpg?c=2]]></isc:image>
			<isc:price><![CDATA[<span class="SalePrice">$200.00</span>]]></isc:price>
			<isc:rating><![CDATA[4]]></isc:rating>
			<isc:rating_image><![CDATA[https://cdn11.bigcommerce.com/r-7f3397d2ae83e8b48dd889540b7b618246f07f43/themes/ClassicNext/images/IcoRating4.gif]]></isc:rating_image>
		</item>
		<item>
			<title><![CDATA[[Sample&91; Orbit Terrarium - Large]]></title>
			<link>https://jewelry5.mybigcommerce.com/orbit-terrarium-large/</link>
			<pubDate>Fri, 03 Jul 2015 18:02:32 +0000</pubDate>
			<guid isPermaLink="false">https://jewelry5.mybigcommerce.com/orbit-terrarium-large/</guid>
			<description><![CDATA[<div style='float: right; padding: 10px;'><a href="https://jewelry5.mybigcommerce.com/orbit-terrarium-large/"><img src="https://cdn11.bigcommerce.com/s-vkapp4pk65/products/80/images/413/11__08909.1492012332.190.285.jpg?c=2" alt="[Sample&91; Orbit Terrarium - Large" /></a></div><p>This strikingly beautiful terrarium will make a welcome addition to your home bringing some green to the scene. A handblown glass sphere rests freely on a thick, concave carved fir base allowing it to be angled in any direction.</p>
<p><em>Plants, rocks and soil are not included.</em></p>
<p>..<p><strong>Price: $109.00</strong> <img src="https://cdn11.bigcommerce.com/r-7f3397d2ae83e8b48dd889540b7b618246f07f43/themes/ClassicNext/images/IcoRating4.gif" alt="" /></p>]]></description>
			<content:encoded><![CDATA[<div style='float: right; padding: 10px;'><a href="https://jewelry5.mybigcommerce.com/orbit-terrarium-large/"><img src="https://cdn11.bigcommerce.com/s-vkapp4pk65/products/80/images/413/11__08909.1492012332.190.285.jpg?c=2" alt="[Sample&91; Orbit Terrarium - Large" /></a></div><p>This strikingly beautiful terrarium will make a welcome addition to your home bringing some green to the scene. A handblown glass sphere rests freely on a thick, concave carved fir base allowing it to be angled in any direction.</p>
<p><em>Plants, rocks and soil are not included.</em></p>
<p>..<p><strong>Price: $109.00</strong> <img src="https://cdn11.bigcommerce.com/r-7f3397d2ae83e8b48dd889540b7b618246f07f43/themes/ClassicNext/images/IcoRating4.gif" alt="" /></p>]]></content:encoded>
			<isc:description><![CDATA[<p>This strikingly beautiful terrarium will make a welcome addition to your home bringing some green to the scene. A handblown glass sphere rests freely on a thick, concave carved fir base allowing it to be angled in any direction.</p>
<p><em>Plants, rocks and soil are not included.</em></p>
<p>..]]></isc:description>
			<isc:productid><![CDATA[80]]></isc:productid>
			<isc:thumb><![CDATA[https://cdn11.bigcommerce.com/s-vkapp4pk65/products/80/images/413/11__08909.1492012296.190.285.jpg?c=2]]></isc:thumb>
			<isc:image><![CDATA[https://cdn11.bigcommerce.com/s-vkapp4pk65/products/80/images/413/11__08909.1492012296.1280.1280.jpg?c=2]]></isc:image>
			<isc:price><![CDATA[$109.00]]></isc:price>
			<isc:rating><![CDATA[4]]></isc:rating>
			<isc:rating_image><![CDATA[https://cdn11.bigcommerce.com/r-7f3397d2ae83e8b48dd889540b7b618246f07f43/themes/ClassicNext/images/IcoRating4.gif]]></isc:rating_image>
		</item>
	</channel>
</rss>
