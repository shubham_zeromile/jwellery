<?xml version="1.0" encoding="UTF-8"?>
<rss version="2.0" xmlns:content="http://purl.org/rss/1.0/modules/content/" xmlns:dc="http://purl.org/dc/elements/1.1/"  xmlns:isc="http://dtd.interspire.com/rss/isc-1.0.dtd">
	<channel>
		<title><![CDATA[Jewelry: Product Search]]></title>
		<link>https://jewelry5.mybigcommerce.com</link>
		<description><![CDATA[A custom search for products at Jewelry.]]></description>
		<pubDate>Fri, 08 May 2020 09:50:24 +0000</pubDate>
		<isc:store_title><![CDATA[Jewelry]]></isc:store_title>
		<item>
			<title><![CDATA[[Sample&91; Utility Caddy]]></title>
			<link>https://jewelry5.mybigcommerce.com/utility-caddy/</link>
			<pubDate>Fri, 03 Jul 2015 18:49:26 +0000</pubDate>
			<guid isPermaLink="false">https://jewelry5.mybigcommerce.com/utility-caddy/</guid>
			<description><![CDATA[<div style='float: right; padding: 10px;'><a href="https://jewelry5.mybigcommerce.com/utility-caddy/"><img src="https://cdn11.bigcommerce.com/s-vkapp4pk65/products/104/images/404/03__13693.1492012135.190.285.jpg?c=2" alt="[Sample&91; Utility Caddy" /></a></div><p>This powder coated steel utility caddy ensures your cleaning essentials are stowed away in one spot ready for your household chores. Brushes, cloths, liquid soaps can all easily be stashed away. Also ideal to be used as a garden caddy to easily grab from the shed for a days work. Works well as ..<p><strong>Price: $45.95</strong> <img src="https://cdn11.bigcommerce.com/r-7f3397d2ae83e8b48dd889540b7b618246f07f43/themes/ClassicNext/images/IcoRating3.gif" alt="" /></p>]]></description>
			<content:encoded><![CDATA[<div style='float: right; padding: 10px;'><a href="https://jewelry5.mybigcommerce.com/utility-caddy/"><img src="https://cdn11.bigcommerce.com/s-vkapp4pk65/products/104/images/404/03__13693.1492012135.190.285.jpg?c=2" alt="[Sample&91; Utility Caddy" /></a></div><p>This powder coated steel utility caddy ensures your cleaning essentials are stowed away in one spot ready for your household chores. Brushes, cloths, liquid soaps can all easily be stashed away. Also ideal to be used as a garden caddy to easily grab from the shed for a days work. Works well as ..<p><strong>Price: $45.95</strong> <img src="https://cdn11.bigcommerce.com/r-7f3397d2ae83e8b48dd889540b7b618246f07f43/themes/ClassicNext/images/IcoRating3.gif" alt="" /></p>]]></content:encoded>
			<isc:description><![CDATA[<p>This powder coated steel utility caddy ensures your cleaning essentials are stowed away in one spot ready for your household chores. Brushes, cloths, liquid soaps can all easily be stashed away. Also ideal to be used as a garden caddy to easily grab from the shed for a days work. Works well as ..]]></isc:description>
			<isc:productid><![CDATA[104]]></isc:productid>
			<isc:thumb><![CDATA[https://cdn11.bigcommerce.com/s-vkapp4pk65/products/104/images/404/03__13693.1492012071.190.285.jpg?c=2]]></isc:thumb>
			<isc:image><![CDATA[https://cdn11.bigcommerce.com/s-vkapp4pk65/products/104/images/404/03__13693.1492012071.1280.1280.jpg?c=2]]></isc:image>
			<isc:price><![CDATA[$45.95]]></isc:price>
			<isc:rating><![CDATA[3]]></isc:rating>
			<isc:rating_image><![CDATA[https://cdn11.bigcommerce.com/r-7f3397d2ae83e8b48dd889540b7b618246f07f43/themes/ClassicNext/images/IcoRating3.gif]]></isc:rating_image>
		</item>
	</channel>
</rss>
