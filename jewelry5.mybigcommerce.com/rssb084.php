<?xml version="1.0" encoding="UTF-8"?>
<rss version="2.0" xmlns:content="http://purl.org/rss/1.0/modules/content/" xmlns:dc="http://purl.org/dc/elements/1.1/"  xmlns:isc="http://dtd.interspire.com/rss/isc-1.0.dtd">
	<channel>
		<title><![CDATA[Jewelry: Product Search]]></title>
		<link>https://jewelry5.mybigcommerce.com</link>
		<description><![CDATA[A custom search for products at Jewelry.]]></description>
		<pubDate>Fri, 08 May 2020 09:46:11 +0000</pubDate>
		<isc:store_title><![CDATA[Jewelry]]></isc:store_title>
		<item>
			<title><![CDATA[[Sample&91; Laundry Detergent]]></title>
			<link>https://jewelry5.mybigcommerce.com/laundry-detergent/</link>
			<pubDate>Fri, 03 Jul 2015 18:40:23 +0000</pubDate>
			<guid isPermaLink="false">https://jewelry5.mybigcommerce.com/laundry-detergent/</guid>
			<description><![CDATA[<div style='float: right; padding: 10px;'><a href="https://jewelry5.mybigcommerce.com/laundry-detergent/"><img src="https://cdn11.bigcommerce.com/s-vkapp4pk65/products/98/images/406/05__86689.1492012159.190.285.jpg?c=2" alt="[Sample&91; Laundry Detergent" /></a></div><p>A 100% biodegradable plant based 3x concentrated laundry detergent for use in conventional and high efficiency machines. This product is leaping bunny certified and has not been tested on animals.</p>
<p>Lavender Scented. Contains 48 High Efficiency Loads.</p>
<p>Free from synthetic fragrance..<p><strong>Price: $29.95</strong> <img src="https://cdn11.bigcommerce.com/r-7f3397d2ae83e8b48dd889540b7b618246f07f43/themes/ClassicNext/images/IcoRating0.gif" alt="" /></p>]]></description>
			<content:encoded><![CDATA[<div style='float: right; padding: 10px;'><a href="https://jewelry5.mybigcommerce.com/laundry-detergent/"><img src="https://cdn11.bigcommerce.com/s-vkapp4pk65/products/98/images/406/05__86689.1492012159.190.285.jpg?c=2" alt="[Sample&91; Laundry Detergent" /></a></div><p>A 100% biodegradable plant based 3x concentrated laundry detergent for use in conventional and high efficiency machines. This product is leaping bunny certified and has not been tested on animals.</p>
<p>Lavender Scented. Contains 48 High Efficiency Loads.</p>
<p>Free from synthetic fragrance..<p><strong>Price: $29.95</strong> <img src="https://cdn11.bigcommerce.com/r-7f3397d2ae83e8b48dd889540b7b618246f07f43/themes/ClassicNext/images/IcoRating0.gif" alt="" /></p>]]></content:encoded>
			<isc:description><![CDATA[<p>A 100% biodegradable plant based 3x concentrated laundry detergent for use in conventional and high efficiency machines. This product is leaping bunny certified and has not been tested on animals.</p>
<p>Lavender Scented. Contains 48 High Efficiency Loads.</p>
<p>Free from synthetic fragrance..]]></isc:description>
			<isc:productid><![CDATA[98]]></isc:productid>
			<isc:thumb><![CDATA[https://cdn11.bigcommerce.com/s-vkapp4pk65/products/98/images/406/05__86689.1492012082.190.285.jpg?c=2]]></isc:thumb>
			<isc:image><![CDATA[https://cdn11.bigcommerce.com/s-vkapp4pk65/products/98/images/406/05__86689.1492012082.1280.1280.jpg?c=2]]></isc:image>
			<isc:price><![CDATA[$29.95]]></isc:price>
			<isc:rating><![CDATA[0]]></isc:rating>
			<isc:rating_image><![CDATA[https://cdn11.bigcommerce.com/r-7f3397d2ae83e8b48dd889540b7b618246f07f43/themes/ClassicNext/images/IcoRating0.gif]]></isc:rating_image>
		</item>
	</channel>
</rss>
