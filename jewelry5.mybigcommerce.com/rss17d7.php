<?xml version="1.0" encoding="UTF-8"?>
<rss version="2.0" xmlns:content="http://purl.org/rss/1.0/modules/content/" xmlns:dc="http://purl.org/dc/elements/1.1/"  xmlns:isc="http://dtd.interspire.com/rss/isc-1.0.dtd">
	<channel>
		<title><![CDATA[Jewelry: Product Search]]></title>
		<link>https://jewelry5.mybigcommerce.com</link>
		<description><![CDATA[A custom search for products at Jewelry.]]></description>
		<pubDate>Fri, 08 May 2020 09:34:20 +0000</pubDate>
		<isc:store_title><![CDATA[Jewelry]]></isc:store_title>
		<item>
			<title><![CDATA[[Sample&91; Smith Journal 13]]></title>
			<link>https://jewelry5.mybigcommerce.com/smith-journal-13/</link>
			<pubDate>Fri, 03 Jul 2015 20:55:30 +0000</pubDate>
			<guid isPermaLink="false">https://jewelry5.mybigcommerce.com/smith-journal-13/</guid>
			<description><![CDATA[<div style='float: right; padding: 10px;'><a href="https://jewelry5.mybigcommerce.com/smith-journal-13/"><img src="https://cdn11.bigcommerce.com/s-vkapp4pk65/products/111/images/402/01__52909.1492012112.190.285.jpg?c=2" alt="[Sample&91; Smith Journal 13" /></a></div><p>Volume 13 of Smith Journal is crammed with more than its fair share of sharp minds. Top of the list would have to be Solomon Shereshevsky, who remembered every single thing he&rsquo;d ever come across &ndash; a great skill to have when it came to party tricks, but enough to send him crackers. A..<p><strong>Price: <span class="SalePrice">$22.00</span></strong> <img src="https://cdn11.bigcommerce.com/r-7f3397d2ae83e8b48dd889540b7b618246f07f43/themes/ClassicNext/images/IcoRating4.gif" alt="" /></p>]]></description>
			<content:encoded><![CDATA[<div style='float: right; padding: 10px;'><a href="https://jewelry5.mybigcommerce.com/smith-journal-13/"><img src="https://cdn11.bigcommerce.com/s-vkapp4pk65/products/111/images/402/01__52909.1492012112.190.285.jpg?c=2" alt="[Sample&91; Smith Journal 13" /></a></div><p>Volume 13 of Smith Journal is crammed with more than its fair share of sharp minds. Top of the list would have to be Solomon Shereshevsky, who remembered every single thing he&rsquo;d ever come across &ndash; a great skill to have when it came to party tricks, but enough to send him crackers. A..<p><strong>Price: <span class="SalePrice">$22.00</span></strong> <img src="https://cdn11.bigcommerce.com/r-7f3397d2ae83e8b48dd889540b7b618246f07f43/themes/ClassicNext/images/IcoRating4.gif" alt="" /></p>]]></content:encoded>
			<isc:description><![CDATA[<p>Volume 13 of Smith Journal is crammed with more than its fair share of sharp minds. Top of the list would have to be Solomon Shereshevsky, who remembered every single thing he&rsquo;d ever come across &ndash; a great skill to have when it came to party tricks, but enough to send him crackers. A..]]></isc:description>
			<isc:productid><![CDATA[111]]></isc:productid>
			<isc:thumb><![CDATA[https://cdn11.bigcommerce.com/s-vkapp4pk65/products/111/images/402/01__52909.1492012060.190.285.jpg?c=2]]></isc:thumb>
			<isc:image><![CDATA[https://cdn11.bigcommerce.com/s-vkapp4pk65/products/111/images/402/01__52909.1492012060.1280.1280.jpg?c=2]]></isc:image>
			<isc:price><![CDATA[<span class="SalePrice">$22.00</span>]]></isc:price>
			<isc:rating><![CDATA[4]]></isc:rating>
			<isc:rating_image><![CDATA[https://cdn11.bigcommerce.com/r-7f3397d2ae83e8b48dd889540b7b618246f07f43/themes/ClassicNext/images/IcoRating4.gif]]></isc:rating_image>
		</item>
		<item>
			<title><![CDATA[[Sample&91; Dustpan & Brush]]></title>
			<link>https://jewelry5.mybigcommerce.com/dustpan-brush/</link>
			<pubDate>Fri, 03 Jul 2015 18:56:16 +0000</pubDate>
			<guid isPermaLink="false">https://jewelry5.mybigcommerce.com/dustpan-brush/</guid>
			<description><![CDATA[<div style='float: right; padding: 10px;'><a href="https://jewelry5.mybigcommerce.com/dustpan-brush/"><img src="https://cdn11.bigcommerce.com/s-vkapp4pk65/products/107/images/403/02__71434.1492012125.190.285.jpg?c=2" alt="[Sample&91; Dustpan &amp; Brush" /></a></div><p>A seemingly simple dustpan with a few features to make life easier. The arch and length of the dustpan eases cleanup, the wood turned handle provides firm grip and the rubber liner along the edge of the scoop will retrieve small crumbs with a single swipe. A key ring at the top makes storage a ..<p><strong>Price: <span class="SalePrice">$33.95</span></strong> <img src="https://cdn11.bigcommerce.com/r-7f3397d2ae83e8b48dd889540b7b618246f07f43/themes/ClassicNext/images/IcoRating0.gif" alt="" /></p>]]></description>
			<content:encoded><![CDATA[<div style='float: right; padding: 10px;'><a href="https://jewelry5.mybigcommerce.com/dustpan-brush/"><img src="https://cdn11.bigcommerce.com/s-vkapp4pk65/products/107/images/403/02__71434.1492012125.190.285.jpg?c=2" alt="[Sample&91; Dustpan &amp; Brush" /></a></div><p>A seemingly simple dustpan with a few features to make life easier. The arch and length of the dustpan eases cleanup, the wood turned handle provides firm grip and the rubber liner along the edge of the scoop will retrieve small crumbs with a single swipe. A key ring at the top makes storage a ..<p><strong>Price: <span class="SalePrice">$33.95</span></strong> <img src="https://cdn11.bigcommerce.com/r-7f3397d2ae83e8b48dd889540b7b618246f07f43/themes/ClassicNext/images/IcoRating0.gif" alt="" /></p>]]></content:encoded>
			<isc:description><![CDATA[<p>A seemingly simple dustpan with a few features to make life easier. The arch and length of the dustpan eases cleanup, the wood turned handle provides firm grip and the rubber liner along the edge of the scoop will retrieve small crumbs with a single swipe. A key ring at the top makes storage a ..]]></isc:description>
			<isc:productid><![CDATA[107]]></isc:productid>
			<isc:thumb><![CDATA[https://cdn11.bigcommerce.com/s-vkapp4pk65/products/107/images/403/02__71434.1492012068.190.285.jpg?c=2]]></isc:thumb>
			<isc:image><![CDATA[https://cdn11.bigcommerce.com/s-vkapp4pk65/products/107/images/403/02__71434.1492012068.1280.1280.jpg?c=2]]></isc:image>
			<isc:price><![CDATA[<span class="SalePrice">$33.95</span>]]></isc:price>
			<isc:rating><![CDATA[0]]></isc:rating>
			<isc:rating_image><![CDATA[https://cdn11.bigcommerce.com/r-7f3397d2ae83e8b48dd889540b7b618246f07f43/themes/ClassicNext/images/IcoRating0.gif]]></isc:rating_image>
		</item>
		<item>
			<title><![CDATA[[Sample&91; Utility Caddy]]></title>
			<link>https://jewelry5.mybigcommerce.com/utility-caddy/</link>
			<pubDate>Fri, 03 Jul 2015 18:49:26 +0000</pubDate>
			<guid isPermaLink="false">https://jewelry5.mybigcommerce.com/utility-caddy/</guid>
			<description><![CDATA[<div style='float: right; padding: 10px;'><a href="https://jewelry5.mybigcommerce.com/utility-caddy/"><img src="https://cdn11.bigcommerce.com/s-vkapp4pk65/products/104/images/404/03__13693.1492012135.190.285.jpg?c=2" alt="[Sample&91; Utility Caddy" /></a></div><p>This powder coated steel utility caddy ensures your cleaning essentials are stowed away in one spot ready for your household chores. Brushes, cloths, liquid soaps can all easily be stashed away. Also ideal to be used as a garden caddy to easily grab from the shed for a days work. Works well as ..<p><strong>Price: $45.95</strong> <img src="https://cdn11.bigcommerce.com/r-7f3397d2ae83e8b48dd889540b7b618246f07f43/themes/ClassicNext/images/IcoRating3.gif" alt="" /></p>]]></description>
			<content:encoded><![CDATA[<div style='float: right; padding: 10px;'><a href="https://jewelry5.mybigcommerce.com/utility-caddy/"><img src="https://cdn11.bigcommerce.com/s-vkapp4pk65/products/104/images/404/03__13693.1492012135.190.285.jpg?c=2" alt="[Sample&91; Utility Caddy" /></a></div><p>This powder coated steel utility caddy ensures your cleaning essentials are stowed away in one spot ready for your household chores. Brushes, cloths, liquid soaps can all easily be stashed away. Also ideal to be used as a garden caddy to easily grab from the shed for a days work. Works well as ..<p><strong>Price: $45.95</strong> <img src="https://cdn11.bigcommerce.com/r-7f3397d2ae83e8b48dd889540b7b618246f07f43/themes/ClassicNext/images/IcoRating3.gif" alt="" /></p>]]></content:encoded>
			<isc:description><![CDATA[<p>This powder coated steel utility caddy ensures your cleaning essentials are stowed away in one spot ready for your household chores. Brushes, cloths, liquid soaps can all easily be stashed away. Also ideal to be used as a garden caddy to easily grab from the shed for a days work. Works well as ..]]></isc:description>
			<isc:productid><![CDATA[104]]></isc:productid>
			<isc:thumb><![CDATA[https://cdn11.bigcommerce.com/s-vkapp4pk65/products/104/images/404/03__13693.1492012071.190.285.jpg?c=2]]></isc:thumb>
			<isc:image><![CDATA[https://cdn11.bigcommerce.com/s-vkapp4pk65/products/104/images/404/03__13693.1492012071.1280.1280.jpg?c=2]]></isc:image>
			<isc:price><![CDATA[$45.95]]></isc:price>
			<isc:rating><![CDATA[3]]></isc:rating>
			<isc:rating_image><![CDATA[https://cdn11.bigcommerce.com/r-7f3397d2ae83e8b48dd889540b7b618246f07f43/themes/ClassicNext/images/IcoRating3.gif]]></isc:rating_image>
		</item>
		<item>
			<title><![CDATA[[Sample&91; Canvas Laundry Cart]]></title>
			<link>https://jewelry5.mybigcommerce.com/canvas-laundry-cart/</link>
			<pubDate>Fri, 03 Jul 2015 18:48:07 +0000</pubDate>
			<guid isPermaLink="false">https://jewelry5.mybigcommerce.com/canvas-laundry-cart/</guid>
			<description><![CDATA[<div style='float: right; padding: 10px;'><a href="https://jewelry5.mybigcommerce.com/canvas-laundry-cart/"><img src="https://cdn11.bigcommerce.com/s-vkapp4pk65/products/103/images/405/04__32061.1492012148.190.285.jpg?c=2" alt="[Sample&91; Canvas Laundry Cart" /></a></div><div>
<p>The last laundry cart you'll ever buy. This industrial non-collapsable cart is constructed with a steel framed body using heavyweight canvas capped with a soft leather rim sitting on four smooth rolling casters. Keeps the grubby garments off the floor and almost makes laundry enjoyable. ..<p><strong>Price: <span class="SalePrice">$200.00</span></strong> <img src="https://cdn11.bigcommerce.com/r-7f3397d2ae83e8b48dd889540b7b618246f07f43/themes/ClassicNext/images/IcoRating4.gif" alt="" /></p>]]></description>
			<content:encoded><![CDATA[<div style='float: right; padding: 10px;'><a href="https://jewelry5.mybigcommerce.com/canvas-laundry-cart/"><img src="https://cdn11.bigcommerce.com/s-vkapp4pk65/products/103/images/405/04__32061.1492012148.190.285.jpg?c=2" alt="[Sample&91; Canvas Laundry Cart" /></a></div><div>
<p>The last laundry cart you'll ever buy. This industrial non-collapsable cart is constructed with a steel framed body using heavyweight canvas capped with a soft leather rim sitting on four smooth rolling casters. Keeps the grubby garments off the floor and almost makes laundry enjoyable. ..<p><strong>Price: <span class="SalePrice">$200.00</span></strong> <img src="https://cdn11.bigcommerce.com/r-7f3397d2ae83e8b48dd889540b7b618246f07f43/themes/ClassicNext/images/IcoRating4.gif" alt="" /></p>]]></content:encoded>
			<isc:description><![CDATA[<div>
<p>The last laundry cart you'll ever buy. This industrial non-collapsable cart is constructed with a steel framed body using heavyweight canvas capped with a soft leather rim sitting on four smooth rolling casters. Keeps the grubby garments off the floor and almost makes laundry enjoyable. ..]]></isc:description>
			<isc:productid><![CDATA[103]]></isc:productid>
			<isc:thumb><![CDATA[https://cdn11.bigcommerce.com/s-vkapp4pk65/products/103/images/405/04__32061.1492012077.190.285.jpg?c=2]]></isc:thumb>
			<isc:image><![CDATA[https://cdn11.bigcommerce.com/s-vkapp4pk65/products/103/images/405/04__32061.1492012077.1280.1280.jpg?c=2]]></isc:image>
			<isc:price><![CDATA[<span class="SalePrice">$200.00</span>]]></isc:price>
			<isc:rating><![CDATA[4]]></isc:rating>
			<isc:rating_image><![CDATA[https://cdn11.bigcommerce.com/r-7f3397d2ae83e8b48dd889540b7b618246f07f43/themes/ClassicNext/images/IcoRating4.gif]]></isc:rating_image>
		</item>
		<item>
			<title><![CDATA[[Sample&91; Laundry Detergent]]></title>
			<link>https://jewelry5.mybigcommerce.com/laundry-detergent/</link>
			<pubDate>Fri, 03 Jul 2015 18:40:23 +0000</pubDate>
			<guid isPermaLink="false">https://jewelry5.mybigcommerce.com/laundry-detergent/</guid>
			<description><![CDATA[<div style='float: right; padding: 10px;'><a href="https://jewelry5.mybigcommerce.com/laundry-detergent/"><img src="https://cdn11.bigcommerce.com/s-vkapp4pk65/products/98/images/406/05__86689.1492012159.190.285.jpg?c=2" alt="[Sample&91; Laundry Detergent" /></a></div><p>A 100% biodegradable plant based 3x concentrated laundry detergent for use in conventional and high efficiency machines. This product is leaping bunny certified and has not been tested on animals.</p>
<p>Lavender Scented. Contains 48 High Efficiency Loads.</p>
<p>Free from synthetic fragrance..<p><strong>Price: $29.95</strong> <img src="https://cdn11.bigcommerce.com/r-7f3397d2ae83e8b48dd889540b7b618246f07f43/themes/ClassicNext/images/IcoRating0.gif" alt="" /></p>]]></description>
			<content:encoded><![CDATA[<div style='float: right; padding: 10px;'><a href="https://jewelry5.mybigcommerce.com/laundry-detergent/"><img src="https://cdn11.bigcommerce.com/s-vkapp4pk65/products/98/images/406/05__86689.1492012159.190.285.jpg?c=2" alt="[Sample&91; Laundry Detergent" /></a></div><p>A 100% biodegradable plant based 3x concentrated laundry detergent for use in conventional and high efficiency machines. This product is leaping bunny certified and has not been tested on animals.</p>
<p>Lavender Scented. Contains 48 High Efficiency Loads.</p>
<p>Free from synthetic fragrance..<p><strong>Price: $29.95</strong> <img src="https://cdn11.bigcommerce.com/r-7f3397d2ae83e8b48dd889540b7b618246f07f43/themes/ClassicNext/images/IcoRating0.gif" alt="" /></p>]]></content:encoded>
			<isc:description><![CDATA[<p>A 100% biodegradable plant based 3x concentrated laundry detergent for use in conventional and high efficiency machines. This product is leaping bunny certified and has not been tested on animals.</p>
<p>Lavender Scented. Contains 48 High Efficiency Loads.</p>
<p>Free from synthetic fragrance..]]></isc:description>
			<isc:productid><![CDATA[98]]></isc:productid>
			<isc:thumb><![CDATA[https://cdn11.bigcommerce.com/s-vkapp4pk65/products/98/images/406/05__86689.1492012082.190.285.jpg?c=2]]></isc:thumb>
			<isc:image><![CDATA[https://cdn11.bigcommerce.com/s-vkapp4pk65/products/98/images/406/05__86689.1492012082.1280.1280.jpg?c=2]]></isc:image>
			<isc:price><![CDATA[$29.95]]></isc:price>
			<isc:rating><![CDATA[0]]></isc:rating>
			<isc:rating_image><![CDATA[https://cdn11.bigcommerce.com/r-7f3397d2ae83e8b48dd889540b7b618246f07f43/themes/ClassicNext/images/IcoRating0.gif]]></isc:rating_image>
		</item>
		<item>
			<title><![CDATA[[Sample&91; Tiered Wire Basket]]></title>
			<link>https://jewelry5.mybigcommerce.com/tiered-wire-basket/</link>
			<pubDate>Fri, 03 Jul 2015 18:36:40 +0000</pubDate>
			<guid isPermaLink="false">https://jewelry5.mybigcommerce.com/tiered-wire-basket/</guid>
			<description><![CDATA[<div style='float: right; padding: 10px;'><a href="https://jewelry5.mybigcommerce.com/tiered-wire-basket/"><img src="https://cdn11.bigcommerce.com/s-vkapp4pk65/products/97/images/415/13__75109.1492012543.190.285.jpg?c=2" alt="[Sample&91; Tiered Wire Basket" /></a></div><p>Here's a three tiered wire basket to clean up your countertop and elevate your storage space. Display a selection of fresh fruit to encourage healthy snacking: pile it high and watch it fly. Can also be used as a caddy for dinner time fixings. Gather up a nice display of baked buns or cupcakes ..<p><strong>Price: $119.95</strong> <img src="https://cdn11.bigcommerce.com/r-7f3397d2ae83e8b48dd889540b7b618246f07f43/themes/ClassicNext/images/IcoRating5.gif" alt="" /></p>]]></description>
			<content:encoded><![CDATA[<div style='float: right; padding: 10px;'><a href="https://jewelry5.mybigcommerce.com/tiered-wire-basket/"><img src="https://cdn11.bigcommerce.com/s-vkapp4pk65/products/97/images/415/13__75109.1492012543.190.285.jpg?c=2" alt="[Sample&91; Tiered Wire Basket" /></a></div><p>Here's a three tiered wire basket to clean up your countertop and elevate your storage space. Display a selection of fresh fruit to encourage healthy snacking: pile it high and watch it fly. Can also be used as a caddy for dinner time fixings. Gather up a nice display of baked buns or cupcakes ..<p><strong>Price: $119.95</strong> <img src="https://cdn11.bigcommerce.com/r-7f3397d2ae83e8b48dd889540b7b618246f07f43/themes/ClassicNext/images/IcoRating5.gif" alt="" /></p>]]></content:encoded>
			<isc:description><![CDATA[<p>Here's a three tiered wire basket to clean up your countertop and elevate your storage space. Display a selection of fresh fruit to encourage healthy snacking: pile it high and watch it fly. Can also be used as a caddy for dinner time fixings. Gather up a nice display of baked buns or cupcakes ..]]></isc:description>
			<isc:productid><![CDATA[97]]></isc:productid>
			<isc:thumb><![CDATA[https://cdn11.bigcommerce.com/s-vkapp4pk65/products/97/images/415/13__75109.1492012536.190.285.jpg?c=2]]></isc:thumb>
			<isc:image><![CDATA[https://cdn11.bigcommerce.com/s-vkapp4pk65/products/97/images/415/13__75109.1492012536.1280.1280.jpg?c=2]]></isc:image>
			<isc:price><![CDATA[$119.95]]></isc:price>
			<isc:rating><![CDATA[5]]></isc:rating>
			<isc:rating_image><![CDATA[https://cdn11.bigcommerce.com/r-7f3397d2ae83e8b48dd889540b7b618246f07f43/themes/ClassicNext/images/IcoRating5.gif]]></isc:rating_image>
		</item>
		<item>
			<title><![CDATA[[Sample&91; Oak Cheese Grater]]></title>
			<link>https://jewelry5.mybigcommerce.com/oak-cheese-grater/</link>
			<pubDate>Fri, 03 Jul 2015 18:31:23 +0000</pubDate>
			<guid isPermaLink="false">https://jewelry5.mybigcommerce.com/oak-cheese-grater/</guid>
			<description><![CDATA[<div style='float: right; padding: 10px;'><a href="https://jewelry5.mybigcommerce.com/oak-cheese-grater/"><img src="https://cdn11.bigcommerce.com/s-vkapp4pk65/products/94/images/408/06__14965.1492012183.190.285.jpg?c=2" alt="[Sample&91; Oak Cheese Grater" /></a></div><p>Crafted from oak and stainless steel this handy cheese grater is the perfect addition for the considered tabletop. Works wonderfully for grated parmesan: the fine stainless steel teeth grate the thing strings of cheese into the drawer below allowing guests to pinch as needed.</p>
<p>Measures 1..<p><strong>Price: $34.95</strong> <img src="https://cdn11.bigcommerce.com/r-7f3397d2ae83e8b48dd889540b7b618246f07f43/themes/ClassicNext/images/IcoRating0.gif" alt="" /></p>]]></description>
			<content:encoded><![CDATA[<div style='float: right; padding: 10px;'><a href="https://jewelry5.mybigcommerce.com/oak-cheese-grater/"><img src="https://cdn11.bigcommerce.com/s-vkapp4pk65/products/94/images/408/06__14965.1492012183.190.285.jpg?c=2" alt="[Sample&91; Oak Cheese Grater" /></a></div><p>Crafted from oak and stainless steel this handy cheese grater is the perfect addition for the considered tabletop. Works wonderfully for grated parmesan: the fine stainless steel teeth grate the thing strings of cheese into the drawer below allowing guests to pinch as needed.</p>
<p>Measures 1..<p><strong>Price: $34.95</strong> <img src="https://cdn11.bigcommerce.com/r-7f3397d2ae83e8b48dd889540b7b618246f07f43/themes/ClassicNext/images/IcoRating0.gif" alt="" /></p>]]></content:encoded>
			<isc:description><![CDATA[<p>Crafted from oak and stainless steel this handy cheese grater is the perfect addition for the considered tabletop. Works wonderfully for grated parmesan: the fine stainless steel teeth grate the thing strings of cheese into the drawer below allowing guests to pinch as needed.</p>
<p>Measures 1..]]></isc:description>
			<isc:productid><![CDATA[94]]></isc:productid>
			<isc:thumb><![CDATA[https://cdn11.bigcommerce.com/s-vkapp4pk65/products/94/images/408/06__14965.1492012092.190.285.jpg?c=2]]></isc:thumb>
			<isc:image><![CDATA[https://cdn11.bigcommerce.com/s-vkapp4pk65/products/94/images/408/06__14965.1492012092.1280.1280.jpg?c=2]]></isc:image>
			<isc:price><![CDATA[$34.95]]></isc:price>
			<isc:rating><![CDATA[0]]></isc:rating>
			<isc:rating_image><![CDATA[https://cdn11.bigcommerce.com/r-7f3397d2ae83e8b48dd889540b7b618246f07f43/themes/ClassicNext/images/IcoRating0.gif]]></isc:rating_image>
		</item>
		<item>
			<title><![CDATA[[Sample&91; 1 L Le Parfait Jar]]></title>
			<link>https://jewelry5.mybigcommerce.com/1-l-le-parfait-jar/</link>
			<pubDate>Fri, 03 Jul 2015 18:29:43 +0000</pubDate>
			<guid isPermaLink="false">https://jewelry5.mybigcommerce.com/1-l-le-parfait-jar/</guid>
			<description><![CDATA[<div style='float: right; padding: 10px;'><a href="https://jewelry5.mybigcommerce.com/1-l-le-parfait-jar/"><img src="https://cdn11.bigcommerce.com/s-vkapp4pk65/products/93/images/409/07__09779.1492012195.190.285.jpg?c=2" alt="[Sample&91; 1 L Le Parfait Jar" /></a></div><p>When translated Le Parfait means "the perfect one" - and that's just what this air-tight jar is. Designed for canning, these jars will ensure your harvest does not spoil, but is kept well-preserved for those cold winter months that lie ahead. Also can be used to store grains, beans and spices. ..<p><strong>Price: <span class="SalePrice">$7.00</span></strong> <img src="https://cdn11.bigcommerce.com/r-7f3397d2ae83e8b48dd889540b7b618246f07f43/themes/ClassicNext/images/IcoRating5.gif" alt="" /></p>]]></description>
			<content:encoded><![CDATA[<div style='float: right; padding: 10px;'><a href="https://jewelry5.mybigcommerce.com/1-l-le-parfait-jar/"><img src="https://cdn11.bigcommerce.com/s-vkapp4pk65/products/93/images/409/07__09779.1492012195.190.285.jpg?c=2" alt="[Sample&91; 1 L Le Parfait Jar" /></a></div><p>When translated Le Parfait means "the perfect one" - and that's just what this air-tight jar is. Designed for canning, these jars will ensure your harvest does not spoil, but is kept well-preserved for those cold winter months that lie ahead. Also can be used to store grains, beans and spices. ..<p><strong>Price: <span class="SalePrice">$7.00</span></strong> <img src="https://cdn11.bigcommerce.com/r-7f3397d2ae83e8b48dd889540b7b618246f07f43/themes/ClassicNext/images/IcoRating5.gif" alt="" /></p>]]></content:encoded>
			<isc:description><![CDATA[<p>When translated Le Parfait means "the perfect one" - and that's just what this air-tight jar is. Designed for canning, these jars will ensure your harvest does not spoil, but is kept well-preserved for those cold winter months that lie ahead. Also can be used to store grains, beans and spices. ..]]></isc:description>
			<isc:productid><![CDATA[93]]></isc:productid>
			<isc:thumb><![CDATA[https://cdn11.bigcommerce.com/s-vkapp4pk65/products/93/images/409/07__09779.1492012099.190.285.jpg?c=2]]></isc:thumb>
			<isc:image><![CDATA[https://cdn11.bigcommerce.com/s-vkapp4pk65/products/93/images/409/07__09779.1492012099.1280.1280.jpg?c=2]]></isc:image>
			<isc:price><![CDATA[<span class="SalePrice">$7.00</span>]]></isc:price>
			<isc:rating><![CDATA[5]]></isc:rating>
			<isc:rating_image><![CDATA[https://cdn11.bigcommerce.com/r-7f3397d2ae83e8b48dd889540b7b618246f07f43/themes/ClassicNext/images/IcoRating5.gif]]></isc:rating_image>
		</item>
		<item>
			<title><![CDATA[[Sample&91; Chemex Coffeemaker 3 Cup]]></title>
			<link>https://jewelry5.mybigcommerce.com/chemex-coffeemaker-3-cup/</link>
			<pubDate>Fri, 03 Jul 2015 18:19:17 +0000</pubDate>
			<guid isPermaLink="false">https://jewelry5.mybigcommerce.com/chemex-coffeemaker-3-cup/</guid>
			<description><![CDATA[<div style='float: right; padding: 10px;'><a href="https://jewelry5.mybigcommerce.com/chemex-coffeemaker-3-cup/"><img src="https://cdn11.bigcommerce.com/s-vkapp4pk65/products/88/images/410/08__03686.1492012207.190.285.jpg?c=2" alt="[Sample&91; Chemex Coffeemaker 3 Cup" /></a></div><p>The Chemex Coffeemaker was created in 1939 by famed inventor Peter J. Schlumbohm.</p>
<p>Applying his knowledge of filtration and extraction, Mr. Schlumbohm was able to craft the vessel that would pour the perfect cup of joe. The angles of the drip, thickness of the filter paper and the air ve..<p><strong>Price: $49.50</strong> <img src="https://cdn11.bigcommerce.com/r-7f3397d2ae83e8b48dd889540b7b618246f07f43/themes/ClassicNext/images/IcoRating0.gif" alt="" /></p>]]></description>
			<content:encoded><![CDATA[<div style='float: right; padding: 10px;'><a href="https://jewelry5.mybigcommerce.com/chemex-coffeemaker-3-cup/"><img src="https://cdn11.bigcommerce.com/s-vkapp4pk65/products/88/images/410/08__03686.1492012207.190.285.jpg?c=2" alt="[Sample&91; Chemex Coffeemaker 3 Cup" /></a></div><p>The Chemex Coffeemaker was created in 1939 by famed inventor Peter J. Schlumbohm.</p>
<p>Applying his knowledge of filtration and extraction, Mr. Schlumbohm was able to craft the vessel that would pour the perfect cup of joe. The angles of the drip, thickness of the filter paper and the air ve..<p><strong>Price: $49.50</strong> <img src="https://cdn11.bigcommerce.com/r-7f3397d2ae83e8b48dd889540b7b618246f07f43/themes/ClassicNext/images/IcoRating0.gif" alt="" /></p>]]></content:encoded>
			<isc:description><![CDATA[<p>The Chemex Coffeemaker was created in 1939 by famed inventor Peter J. Schlumbohm.</p>
<p>Applying his knowledge of filtration and extraction, Mr. Schlumbohm was able to craft the vessel that would pour the perfect cup of joe. The angles of the drip, thickness of the filter paper and the air ve..]]></isc:description>
			<isc:productid><![CDATA[88]]></isc:productid>
			<isc:thumb><![CDATA[https://cdn11.bigcommerce.com/s-vkapp4pk65/products/88/images/410/08__03686.1492012105.190.285.jpg?c=2]]></isc:thumb>
			<isc:image><![CDATA[https://cdn11.bigcommerce.com/s-vkapp4pk65/products/88/images/410/08__03686.1492012105.1280.1280.jpg?c=2]]></isc:image>
			<isc:price><![CDATA[$49.50]]></isc:price>
			<isc:rating><![CDATA[0]]></isc:rating>
			<isc:rating_image><![CDATA[https://cdn11.bigcommerce.com/r-7f3397d2ae83e8b48dd889540b7b618246f07f43/themes/ClassicNext/images/IcoRating0.gif]]></isc:rating_image>
		</item>
		<item>
			<title><![CDATA[[Sample&91; Able Brewing System]]></title>
			<link>https://jewelry5.mybigcommerce.com/able-brewing-system/</link>
			<pubDate>Fri, 03 Jul 2015 18:16:02 +0000</pubDate>
			<guid isPermaLink="false">https://jewelry5.mybigcommerce.com/able-brewing-system/</guid>
			<description><![CDATA[<div style='float: right; padding: 10px;'><a href="https://jewelry5.mybigcommerce.com/able-brewing-system/"><img src="https://cdn11.bigcommerce.com/s-vkapp4pk65/products/86/images/411/09__12181.1492012307.190.285.jpg?c=2" alt="[Sample&91; Able Brewing System" /></a></div><p>Stemming from an intense passion for the most flavourful cup of coffee, Able Brewing set out to create a brewer that was as aesthetically pleasing as it was functional. They imagined a product that would easily find itself at home in your kitchen during your morning routine. A product that woul..<p><strong>Price: $225.00</strong> <img src="https://cdn11.bigcommerce.com/r-7f3397d2ae83e8b48dd889540b7b618246f07f43/themes/ClassicNext/images/IcoRating0.gif" alt="" /></p>]]></description>
			<content:encoded><![CDATA[<div style='float: right; padding: 10px;'><a href="https://jewelry5.mybigcommerce.com/able-brewing-system/"><img src="https://cdn11.bigcommerce.com/s-vkapp4pk65/products/86/images/411/09__12181.1492012307.190.285.jpg?c=2" alt="[Sample&91; Able Brewing System" /></a></div><p>Stemming from an intense passion for the most flavourful cup of coffee, Able Brewing set out to create a brewer that was as aesthetically pleasing as it was functional. They imagined a product that would easily find itself at home in your kitchen during your morning routine. A product that woul..<p><strong>Price: $225.00</strong> <img src="https://cdn11.bigcommerce.com/r-7f3397d2ae83e8b48dd889540b7b618246f07f43/themes/ClassicNext/images/IcoRating0.gif" alt="" /></p>]]></content:encoded>
			<isc:description><![CDATA[<p>Stemming from an intense passion for the most flavourful cup of coffee, Able Brewing set out to create a brewer that was as aesthetically pleasing as it was functional. They imagined a product that would easily find itself at home in your kitchen during your morning routine. A product that woul..]]></isc:description>
			<isc:productid><![CDATA[86]]></isc:productid>
			<isc:thumb><![CDATA[https://cdn11.bigcommerce.com/s-vkapp4pk65/products/86/images/411/09__12181.1492012279.190.285.jpg?c=2]]></isc:thumb>
			<isc:image><![CDATA[https://cdn11.bigcommerce.com/s-vkapp4pk65/products/86/images/411/09__12181.1492012279.1280.1280.jpg?c=2]]></isc:image>
			<isc:price><![CDATA[$225.00]]></isc:price>
			<isc:rating><![CDATA[0]]></isc:rating>
			<isc:rating_image><![CDATA[https://cdn11.bigcommerce.com/r-7f3397d2ae83e8b48dd889540b7b618246f07f43/themes/ClassicNext/images/IcoRating0.gif]]></isc:rating_image>
		</item>
	</channel>
</rss>
